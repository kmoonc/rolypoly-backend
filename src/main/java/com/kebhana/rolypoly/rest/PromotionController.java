package com.kebhana.rolypoly.rest;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.kebhana.rolypoly.dao.PromotionSystemDao;
import com.kebhana.rolypoly.model.CustBase;
import com.kebhana.rolypoly.model.Hello;
import com.kebhana.rolypoly.model.Promotion;
import com.kebhana.rolypoly.model.Transaction;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Api(value="Promotion Service API")
@RestController
public class PromotionController {
	private String msgTemplate = "%s님  반갑습니다.";
	private final AtomicLong  vistorConouter = new AtomicLong();
	
	@Autowired
	private PromotionSystemDao promotionSystemDao;
		

//	@ApiOperation(value="Promotion API")
//	@ApiImplicitParams({
//		@ApiImplicitParam(name="name", value="이름 ",required = true, dataType="String", paramType="query")
//	})
//	@RequestMapping(value="/promotion", method=RequestMethod.GET)
//	public Hello getHelloMsg(@RequestParam(value="name" ,required = true) String name ) {
//		return new Hello(vistorConouter.incrementAndGet(), String.format(msgTemplate, name));
//	}
	
	@ApiOperation(value="고객별 추천 프로모션 조회")
	@RequestMapping(value="/promotion/{custNo}", method=RequestMethod.GET)
	public ResponseEntity<List<Promotion>> getPromotionsByCustNo(
			@PathVariable (name="custNo", required = true) String custNo
			) { 
		
		List<Promotion> list = null;
		try {
			log.info("고객별 추천 프로모션 조회 db select");
			list = promotionSystemDao.getPromotions(custNo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.debug("Promotion counts :"+list.size());
		
		return new ResponseEntity<List<Promotion>> (list, HttpStatus.OK);

	}
	
	@ApiOperation(value="특정 프로모션 정보 조회")
	@RequestMapping(value="/promotion/event/{eventId}", method=RequestMethod.GET)
	public ResponseEntity<Promotion> getPromotionDetail(
			@PathVariable (name="eventId", required = true) String eventId
			) { 
		
		Promotion promotion = null;
		try {
			log.info("고객별 추천 프로모션 조회 select");
			promotion = promotionSystemDao.getPromotionDetail(eventId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return new ResponseEntity<Promotion> (promotion, HttpStatus.OK);

	}
	
	@ApiOperation(value="고객별 거래내역 조회")
	@RequestMapping(value="/transaction/{custNo}", method=RequestMethod.GET)
	public ResponseEntity<List<Transaction>> getTransactionByCustNo(
			@PathVariable (name="custNo", required = true) String custNo
			) { 
		
		List<Transaction> list = null;

		try {
			log.info("전체고객정보 조회 db select");
			list = promotionSystemDao.selectTrscByCustNo(custNo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.debug("user counts :"+list.size());
		
		return new ResponseEntity<List<Transaction>> (list, HttpStatus.OK);
	}
	
	@ApiOperation(value="전체고객정보 조회")
	@RequestMapping(value="/cust/list", method=RequestMethod.GET)
	public ResponseEntity <List<CustBase>> getCustList() { 
		
		List<CustBase> list = null;
		try {
			log.info("전체고객정보 조회 db select");
			list = promotionSystemDao.selectCust();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.debug("user counts :"+list.size());
		
		return new ResponseEntity<List<CustBase>> (list, HttpStatus.OK);
	}
	
	@ApiOperation(value="특정고객정보 조회")
	@RequestMapping(value="/cust/{custNo}", method=RequestMethod.GET)
	public ResponseEntity <CustBase> getCustBase(
			@PathVariable (name="custNo", required = true) String custNo
			) { 
		
		CustBase cust = null;
		try {
			log.info("특정고객정보 조회 db select");
			cust = promotionSystemDao.selectCustByCustNo(custNo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<CustBase> (cust, HttpStatus.OK);

	}
	
	@ApiOperation(value="고객정보 변경")
	@RequestMapping(value="/cust/{custNo}", method=RequestMethod.PUT)
	public ResponseEntity <String> setUserUpdate(
		@PathVariable(name="custNo",required = true ) String custNo,
		@RequestBody CustBase cust
		) throws Exception { 

		log.info("고객정보 변경 db update");
		cust.setCustNo(custNo);
		int re  = promotionSystemDao.updateCust(cust);
		log.debug("result :"+ re);
	
		return new ResponseEntity<String> (re+"", HttpStatus.OK);
	}
	 
	
//	@ApiOperation(value="아이디로 사용자 정보 가져오기 ")
//	@RequestMapping(value="/users/{userId}", method=RequestMethod.GET)
//	public ResponseEntity <SampleUser> getUsserById(
//				@PathVariable (name="userId", required = true) String userId
//			) { 
//		SampleUser re = null;
//		try {
//			log.info("Start db select");
//			re = sampleUserDao.selectUserById(userId);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return new ResponseEntity<SampleUser> (re, HttpStatus.OK);
//	}
//	
//	@ApiOperation(value="사용자 정보 변경하기 ")
//	@RequestMapping(value="/users/{userId}", method=RequestMethod.PUT)
//	public ResponseEntity <String > setUserUpdate(
//			@PathVariable(name="userId",required = true ) String userId, 
//			@RequestBody SampleUser sampleUer
//		) throws Exception { 
//		
//		List<SampleUser> list = null;
//		log.info("Start db update");
//		sampleUer.setUserId(userId);
//		int re  = sampleUserDao.updateUser(sampleUer);
//		log.debug("result :"+ re);
//		
//		return new ResponseEntity<String> (re+"", HttpStatus.OK);
//	}
//	
//	@ApiOperation(value="사용자 정보 등록하기 ")
//	@RequestMapping(value="/users", method=RequestMethod.POST)
//	public ResponseEntity <String > setUserInsert(
//			@RequestBody SampleUser sampleUer
//		) throws Exception { 
//		
//		List<SampleUser> list = null;
//		log.info("Start db insert");
//		int re  = sampleUserDao.insertUser(sampleUer);
//		log.debug("result :"+ re);
//		
//		return new ResponseEntity<String> (re+"", HttpStatus.OK);
//	}
//	
//	@ApiOperation(value="사용자 정보 삭제하기 ")
//	@RequestMapping(value="/users/{userId}", method=RequestMethod.DELETE)
//	public ResponseEntity <String > setUserDelete(
//			@PathVariable(name="userId",required = true ) String userId
//		) throws Exception { 
//		
//		List<SampleUser> list = null;
//		log.info("Start db insert");
//		int re  = sampleUserDao.deleteUser(userId);
//		log.debug("result :"+ re);
//		
//		return new ResponseEntity<String> (re+"", HttpStatus.OK);
//	}
	
	@ApiOperation(value="DB 정상연결여부 테스트")
	@RequestMapping(value="/test/db", method=RequestMethod.GET)
	public ResponseEntity <String> getUserList2() { 
		
		int test = -1;
		try {
			log.info("Start db select");
			test = promotionSystemDao.selectTest();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.debug("user counts :"+test);
		
		return new ResponseEntity<String> (test+"", HttpStatus.OK);
	}

}
