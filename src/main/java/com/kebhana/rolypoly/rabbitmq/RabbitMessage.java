package com.kebhana.rolypoly.rabbitmq;

import lombok.Data;

@Data
public class RabbitMessage<T> {
	private T t;
}
