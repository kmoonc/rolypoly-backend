package com.kebhana.rolypoly.rabbitmq;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import lombok.extern.slf4j.Slf4j;

import com.kebhana.rolypoly.dao.PromotionSystemDao;
import com.kebhana.rolypoly.model.CustBase;
import com.kebhana.rolypoly.model.Transaction;

@Component
@Slf4j
public class BroadcastMessageConsumers {
	
	@Autowired
	private PromotionSystemDao promotionSystemDao;
	
	//수신서비스 
	@RabbitListener(queues = "${prop.rabbit.queue.order}" )
	public void receiveMessageFromDirectExchangeWithOrderQueue(Transaction message) throws Exception {
		log.debug("CHARGE_ORDER_QUEUE Receive : "+message.toString());
		
		//Transaction data를 수신해서 로직 처리
		log.info("Transaction 등록 db insert");
		int re  = promotionSystemDao.insertTrsc(message);
		log.debug("result :"+ re);
	}
	
//?? 먼가 있을듯한데..
//	@RabbitListener(queues = {CHARGE_COMPLETE_QUEUE})
//	public void receiveMessageFromDirectExchangeWithCompleteQueue(SampleUser message) {
//		log.debug(" CHARGE_COMPLETE_QUEUE Receive : "+message.toString());
//	}
}
