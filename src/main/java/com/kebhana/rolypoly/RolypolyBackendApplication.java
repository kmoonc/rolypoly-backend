package com.kebhana.rolypoly;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RolypolyBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(RolypolyBackendApplication.class, args);
	}

}
