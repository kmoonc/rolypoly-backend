package com.kebhana.rolypoly.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import com.kebhana.rolypoly.model.CustBase;
import com.kebhana.rolypoly.model.Promotion;
import com.kebhana.rolypoly.model.Transaction;


@Mapper
public interface PromotionSystemDao {

	/**
	 * 사용자 전체 정보 가져오기 
	 * @return
	 * @throws Exception
	 */
	List<CustBase> selectCust() throws Exception;
	
	CustBase selectCustByCustNo(String custNo) throws Exception;
	
	int updateCust(CustBase cust) throws Exception;
	
	List<Promotion> getPromotions(String custNo) throws Exception;
	
	Promotion getPromotionDetail(String eventId) throws Exception;
	
	int insertTrsc(Transaction trsc) throws Exception;
	
	List<Transaction> selectTrscByCustNo(String custNo) throws Exception;
	
	int selectTest() throws Exception;		
}
			