package com.kebhana.rolypoly.model;

import lombok.Data;

@Data
public class CustBase {
	private String custNo ; //고객번호
	private String custNm ; //고객명
	private String birthDt ; //생년월일
	private String sex ; //성별
}
