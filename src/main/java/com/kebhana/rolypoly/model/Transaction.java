package com.kebhana.rolypoly.model;

import lombok.Data;

@Data
public class Transaction {
	private String custNo ; //고객번호
	private String trscTyp ; //거래유형
	private int trscSeqNo ; //거래일련번호
	private String trscDt ; //거래일자
	private String contDtlsNo ; //계약상세번호
	private String addCond ; //추가조건
}

//	{
//	  "addCond": "",
//	  "contDtlsNo": "10181029833907",
//	  "custNo": "000000001",
//	  "trscDt": "20191223",
//	  "trscSeqNo": 0,
//	  "trscTyp": "스타벅스결제"
//	}