package com.kebhana.rolypoly.model;

import lombok.Data;

@Data
public class Promotion {
	private String eventId ; //이벤트ID
	private String eventNm ; //이벤트명
	private String notiCtt ; //고객안내내용
	private String notiDtlsCtt ; //고객안내상세내용
	private String urlCtt ; //연동URL
	private String imgFileNm ; //이미지파일
}
